<?php 
if (isset($_GET['delete']))
{
    $prekiumasyvas = json_decode(file_get_contents('./prekes.json'), JSON_OBJECT_AS_ARRAY); //decode pavercia is json i php masyvo tipa.Jei nenurodai objekto, grazina reiksmes buvusias.
    /*foreach ($prekiumasyvas as $preke) {
        if($preke->ID == $_GET['delete']) {
            istrinti_dbaze($preke->ID);
            break;
        }
    }*/
    unset($prekiumasyvas[$_GET['delete']]);
    //var_dump($prekiumasyvas);
    $prekiumasyvas = array_values($prekiumasyvas);
    if (is_writable('./prekes.json'))
    {
        $ifaila = json_encode($prekiumasyvas);
        file_put_contents('./prekes.json', $ifaila);
    }
}

header("Location: ./administracinis_sarasas.php");
