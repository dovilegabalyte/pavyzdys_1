<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device, initial-scale=1.0">
    <title>Documentai</title>
</head>
<body>
    <h2> Prekių sąrašas</h2>
    <?php 
        session_start();

        if(file_exists('./prekes.json')) {
            if(is_readable('./prekes.json')) {
                $contents = file_get_contents('./prekes.json');
                $array = json_decode($contents, true);
 
                if (isset($_GET['delele'])) {
                    unset($array[$_GET['delele']]);
                    $iJSON = json_encode($array, true, JSON_PRETTY_PRINT);
                    file_put_contents('./prekes.json', $iJSON);
                }
            }
        }
        // $_SESSION Wishlist sukūrimas
        if (!isset($_SESSION["wishlist"])) {
            $_SESSION["wishlist"] = array();
        }
        if (!is_array($_SESSION["wishlist"]))
        {
            $_SESSION["wishlist"] = array();
        }

        // Pridėjimas į $_SESSION['wishlist'] masyvą
        if (isset($_GET['add'])) {
            $prideti = null;
            foreach ($array as $preke) {
                if ($preke['id'] == $_GET['add']) {
                    $prideti = $preke;
                    break;
                }
            }
            if ($prideti != null) {
                $duplicate = false;
                foreach ($_SESSION["wishlist"] as $w) {
                    if ($w['id'] == $prideti['id']) {
                        $duplicate = true;
                    }
                }
                if (!$duplicate) {
                    if ($prideti !== null) {
                        array_push(
                        $_SESSION["wishlist"],
                        [
                            "id" => $prideti['id'],
                            "preke" => $prideti['preke'],
                            "kaina" => $prideti['kaina'],
                        ]
                    );
                    }
                }
            }
        }
        
        if (isset($_GET['delete']) && isset($_SESSION['wishlist'][$_GET['delete']]) )
        {
            unset($_SESSION['wishlist'][$_GET['delete']]);
        }

    ?>

    <table border="1px">
        <tr>
            <th>ID</th>
            <th>Preke</th>
            <th>Kaina</th>
            <th>Pridėti</th>
        </tr>
        <?php if(!empty($array))foreach($array as $key => $value) : ?>
            <tr>
                <td><?php echo $value['id']?></td>
                <td><?php echo $value['preke']?></td>
                <td><?php echo $value['kaina']?></td>
                <td><a href=<?php echo "\"?add=" .$value['id'] . "\""; ?>> <center> &#9829; </center></td>
            </tr>
            <?php endforeach;
            else { ?>
                <td colspan="3"><?php echo "Jūsų norų sąrašas yra tuščias" ?></td>
            <?php } ?>
    
    </table>

    <hr>

    <h2> Norų sąrašas </h2>
    <?php if (!empty($_SESSION["wishlist"]))
    { 
        ?>
        <table border="1px">
        <tr>
            <th></th>
            <th>Preke</th>
            <th>Kaina</th>
            <th>Istrinti</th>
        </tr>
        <?php foreach ($_SESSION["wishlist"] as $key => $value):?>
            <tr>
                <td> <?php echo $value['id']+1; ?></th>
                <td> <?php echo $value["preke"]; ?></td>
                <td> <?php echo $value["kaina"]; ?></td>
                <td> <a href=<?php echo "\"?delete=" .$key . "\""; ?>> <center> X </center> </td>
            </tr>
        <?php endforeach; ?>
        </table>
    <?php
    }
    else echo "Šiuo metu jūsų wishlist yra tuščias."; ?>

</body>
</html>