<?php
$prekiumasyvas = json_decode(file_get_contents('./prekes.json'), JSON_OBJECT_AS_ARRAY); //decode pavercia is json i php masyvo tipa.Jei nenurodai objekto, grazina reiksmes buvusias.
$preke = $prekiumasyvas[$_GET['redaguoti']];
if (isset($_GET['preke']) && isset($_GET['kaina']) && $preke != null) {

    //redaguoti_db($preke->ID, trim($_GET['preke']), trim ($_GET['kaina']));
    $prekiumasyvas[$_GET['redaguoti']]['preke']= trim ($_GET['preke']);
    $prekiumasyvas[$_GET['redaguoti']]['kaina']= trim ($_GET['kaina']);
    if (is_writable('./prekes.json')) {
        $ifaila = json_encode($prekiumasyvas);
        file_put_contents('./prekes.json', $ifaila);
    }
    header("Location: ./administracinis_sarasas.php");

}
?>

<?php if (isset($_GET['redaguoti']) && isset($prekiumasyvas[$_GET['redaguoti']])): $redirect=false; ?>
<form>
    <input  type='hidden' 
            value='<?php echo $_GET['redaguoti']; ?>' 
            name='redaguoti' />
    <input  type='text' 
            value='<?php echo $preke['preke']; ?>'
            name='preke'/>
    <input  type='number' step="0.01"
            value='<?php echo $preke['kaina']; ?>'
            name='kaina'/>


    <input  type='submit' />
</form>
<?php endif; 


?>    